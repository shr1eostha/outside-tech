$(document).ready(function(){

    // Sticky Header Js
    $(window).scroll(function() {
        if ($(this).scrollTop() > 50){  
            $('.site-header').addClass("sticky");
        }
        else {
            $('.site-header').removeClass("sticky");
        }
    });

    // Navbar Toggle Js
    $('.navbar-toggle').click(function(){
        $('.site-header .site-navigation').addClass('open');
        $('#backdrop').addClass('open');
    });

    // Navbar Close Js
    $('.navbar-close').click(function(e){
        $('.site-header .site-navigation').removeClass('open');
        $('#backdrop').removeClass('open');
    });

    // Backdrop Js
    $('#backdrop').click(function(){
        $('.site-header .site-navigation').removeClass('open');
        $(this).removeClass('open');
    });

    // Homeslider JS 
    $('.homeslider').bxSlider({
        controls: false,
        pause: 20000,
        auto: true,
        speed: 2000,
        loop: true,
        pager: true,
        pagerSelector: '#pager1'
    });


    // GSAP Animation Js 
    var controller = new ScrollMagic.Controller();

    $('section').each(function(){
        var mainwrapper = $(this).find(".animation");
        var heading = $(this).find(".animation h1");
        var headingthree = $(this).find(".animation h3");
        var para = $(this).find(".animation p");
        var clickbtn = $(this).find(".animation .site-btn");
        var bottlefigure = $(this).find(".bottlefigure");
        var animateIn = new TimelineMax();
    
        animateIn.fromTo(mainwrapper, 2, {skewX: 0, scale: 1, xPercent: -2}, {skewX: 0, xPercent: 1,
        transformOrigin: "0% 100%", ease: Power4.easeOut });
        animateIn.from(heading, 0.5, {scaleY: 0, transformOrigin: "bottom left"},
        "-=1.5");
        animateIn.from(headingthree, 1, {autoAlpha: 0, x: 30, ease: Expo.easeOut}, "-=1");
        animateIn.from(para, 0.8, {autoAlpha: 0, y: 30, ease: Power4.easeOut}, "-=0.8");
        animateIn.from(bottlefigure, 0.8, {autoAlpha: 0, y: 30, ease: Power2.easeOut, delay: 1.5}, "-=1.5");
        animateIn.from(clickbtn, 0.5, {autoAlpha: 0, y: 30, ease: Power4.easeOut}, "-=0.5");


        // Make a scrollmagic scene
        var scene = new ScrollMagic.Scene({
            triggerElement: this,
        })

        .setTween(animateIn).addTo(controller);
        
    });


});


