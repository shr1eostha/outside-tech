const { series } = require('gulp');
const gulp = require('gulp');
const sass = require('gulp-sass');
const del = require('del');
const browserSync = require('browser-sync').create();
var uglify = require('gulp-uglify-es').default; // Minification for ES6
var concat = require('gulp-concat');


// Compiling CSs
gulp.task('styles', () => {
    return gulp.src('assets/scss/style.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('assets/css/'))
        .pipe(browserSync.stream());
});

// Concatenation & Minification Scripts
gulp.task('scripts', function() {
    return gulp.src(['assets/dist/jquery.js', 'assets/dist/popper.js', 'assets/dist/bootstrap.js', 'assets/dist/jquery.bxslider.js', 'assets/dist/gsap.min.js', 'assets/dist/scrollmagic.min.js', 'assets/dist/animations.gsap.min.js', 'assets/dist/addindicators.js', 'assets/dist/timelinemax.min.js', 'assets/dist/main.js'])
    .pipe(concat('all.js'))
    .pipe(uglify())
    .pipe(gulp.dest('assets/js/'));
});


// Remove Styles & Scripts
gulp.task('clean', () => {
    return del([
        'assets/css/style.css',
        'assets/js/all.js'
    ]);
});


// Watch for changes
gulp.task('watch', () => {
    gulp.watch('assets/scss/*.scss', (done) => {
        gulp.series(['clean', 'styles'])(done);
    });

    gulp.watch('assets/dist/*.js', (done) => {
        gulp.series(['clean', 'scripts'])(done);
    });
});


// Export default task
gulp.task('default', gulp.series(['watch']));